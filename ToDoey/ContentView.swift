//
//  ContentView.swift
//  ToDoey
//
//  Created by Rime on 16/05/2023.
//

import SwiftUI


struct ContentView: View {
    @State private var title = "To DO"
    let toDoArray = ["item1","item2","item3"]
    
    var body: some View {
        NavigationView {
            List {
                ForEach(toDoArray, id: \.self) {
                    todo in
                    NavigationLink(destination: DetailView()) {
                        Text(todo)
                    }
                }
                
            }
            .navigationTitle("To Do")
            .toolbar {
                Button("Add") {
                    
                    
                }
                    
            }
        }
    }
    
    struct ContentView_Previews: PreviewProvider {
        static var previews: some View {
            ContentView()
        }
    }
}
