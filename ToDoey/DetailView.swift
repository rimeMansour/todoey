//
//  DetailView.swift
//  ToDoey
//
//  Created by Rime on 16/05/2023.
//

import SwiftUI

struct DetailView: View {
    var body: some View {
        VStack (alignment:.leading){
          Spacer()
            HStack{
                Text("dd/mm/yy")
                    .foregroundColor(.gray)
                Image(systemName: "checkmark")
                    .foregroundColor(.blue)
            }
            Spacer()
            
            
            Text("rhewuhrewrewrwbruewbrhiwuyrgeuyrguyergwgrgewuy ruy uyrbuyrb vuyrbuyrbyewb uy buewbruybruywbruew brhewbrywebrywebr yehrb ywe bryew bryewb rywb ryhew bryhrwy")
                
            Spacer()
        }
        .padding()
        
        .navigationTitle("Title")
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView()
    }
}
