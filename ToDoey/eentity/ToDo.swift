//
//  ToDo.swift
//  ToDoey
//
//  Created by Rime on 16/05/2023.
//

import Foundation

struct ToDo: Identifiable {
    let id = UUID()
    let title : String
    let date : Int
    let body : String
    let isChecked : Bool

}
