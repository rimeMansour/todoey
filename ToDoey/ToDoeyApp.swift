//
//  ToDoeyApp.swift
//  ToDoey
//
//  Created by Rime on 16/05/2023.
//

import SwiftUI

@main
struct ToDoeyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
